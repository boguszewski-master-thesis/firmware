/*
 * frame.h
 *
 *  Created on: 16 Nov 2021
 *      Author: robsk
 */

#ifndef INC_FRAME_H_
#define INC_FRAME_H_

#include "compressor.h"

/**
 * @brief command sended to device
 */
typedef enum cmd{
	uart_cmd_lz77,
	uart_cmd_lzss
}cmd_t;

/**
 * @brief subcommand sended to device
 */
typedef union sub_cmd{
	lz77_sub_cmd_t lz77;
	lzss_sub_cmd_t lzss;
}sub_cmd_t;

/**
 *  @brief comunication frame
 */
typedef struct frame_struct{

	struct{
		cmd_t cmd;
		sub_cmd_t sub_cmd;
	}head;

	uint8_t *data;
	uint8_t crc;
}frame_struct_t;

#endif /* INC_FRAME_H_ */
