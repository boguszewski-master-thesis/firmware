/*
 * uart_transmitter.c
 *
 *  Created on: 15 Nov 2021
 *      Author: robsk
 */
#include "uart_transmitter.h"

#include "cmsis_os.h"

#include "frame.h"
#include "usart.h"

static void init(void);

osSemaphoreDef(send_data_request);
osSemaphoreDef(tx_transmit_complete);

static struct uart_transmitter_module{
	osSemaphoreId send_data_request;	// Semaphore use to send data by module
	osSemaphoreId tx_transmit_complete;

	frame_struct_t frame;
	size_t	count_data;
}module;

uint16_t CRC16_2(uint8_t *buf, int len)
{  
  uint16_t crc = 0xFFFF;
  for (int pos = 0; pos < len; pos++)
  {
  crc ^= (uint16_t)buf[pos];    // XOR byte into least sig. byte of crc

  for (int i = 8; i != 0; i--) {    // Loop over each bit
    if ((crc & 0x0001) != 0) {      // If the LSB is set
      crc >>= 1;                    // Shift right and XOR 0xA001
      crc ^= 0xA001;
    }
    else                            // Else LSB is not set
      crc >>= 1;                    // Just shift right
    }
  }

  return crc;
}

uint16_t crc;

void uart_transmitter_task(void const *args)
{
	init();

	while(1)
	{
		osSemaphoreWait(module.send_data_request, osWaitForever);

		crc = CRC16_2(module.frame.data, module.count_data);

		usart_send_data((uint8_t * )&module.frame.head, sizeof(module.frame.head));
		osSemaphoreWait(module.tx_transmit_complete, osWaitForever);

		usart_send_data(module.frame.data, module.count_data);
		osSemaphoreWait(module.tx_transmit_complete, osWaitForever);

		usart_send_data((uint8_t *)&crc, sizeof(uint16_t));
		osSemaphoreWait(module.tx_transmit_complete, osWaitForever);
	}
}

void uart_transmitter_send(cmd_t cmd, sub_cmd_t sub_cmd, uint8_t *data, size_t count_data)
{
	module.frame.head.cmd 		= cmd;
	module.frame.head.sub_cmd 	= sub_cmd;
	module.frame.data			= data;
	module.count_data			= count_data;

	osSemaphoreRelease(module.send_data_request);
}

inline void uart_transmitter_tx_complete(void)
{
	osSemaphoreRelease(module.tx_transmit_complete);
}

inline static void init(void)
{
	module.send_data_request 	= osSemaphoreCreate(osSemaphore(send_data_request), 1);
	module.tx_transmit_complete	= osSemaphoreCreate(osSemaphore(tx_transmit_complete), 1);

	osSemaphoreWait(module.send_data_request, osWaitForever);
	osSemaphoreWait(module.tx_transmit_complete, osWaitForever);
}
