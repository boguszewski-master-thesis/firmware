/*
 * heart_beater.c
 *
 *  Created on: Nov 11, 2021
 *      Author: robsk
 */
#include "heart_beater.h"

#include "main.h"
#include "cmsis_os.h"

/**
 * @brief task only to show if mcu works and RTOS is not blocked
 */
void heart_beater_task(void const *args)
{
	while(1)
	{
		LD4_GPIO_Port->ODR ^= LD4_Pin;
		osDelay(500);
	}
}
