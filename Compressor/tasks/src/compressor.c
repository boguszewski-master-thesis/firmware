/*
 * compressor.c
 *
 *  Created on: 12 Nov 2021
 *      Author: robsk
 */
#include "compressor.h"

#include "cmsis_os.h"
#include "lz77.h"
#include "lzss.h"
#include "uart_receiver.h"
#include "frame.h"
#include "uart_transmitter.h"

static void init(void);
static void lz77_service(void);
static void lzss_service(void);

static const uint8_t MESSAGE_OK[] 		= "OK";
static const uint8_t MESSAGE_ERROR[] 	= "ERROR";

osSemaphoreDef(DATA_TO_COMPRESS_RECEIVED);

typedef union compressor_sub_cmd{
	lz77_sub_cmd_t lz77;
	lzss_sub_cmd_t lzss;
}compressor_sub_cmd_t;

static struct{
	osSemaphoreId cmd_request; 		// semaphore wait for command receiving

	//Input data to module
	volatile struct{
		cmd_t	cmd;
		compressor_sub_cmd_t sub_cmd;
		uint8_t *data;
		size_t count;

	}input;

	//Output data from module
	struct{
		uint8_t data[900];
		size_t 	count;
	}output;

	lz77_handle_t lz77_handle;
	lzss_handle_t lzss_handle;
}module;

size_t received_data = 0;

/**
 * 	@brief task used to compress data, use diffrent algorithm
 */
void compressor_task(void const *args)
{
	init();

	while(1)
	{
		osSemaphoreWait(module.cmd_request, osWaitForever);

		if(module.input.cmd == uart_cmd_lz77) lz77_service();
		if(module.input.cmd == uart_cmd_lzss) lzss_service();
	}
}

/**
 * 	@brief cmd request sended to this module, use to manipulate LZ77 compressor
 * 	@param sub_cmd sub command when
 * 			compressor_cmd_lz77_start 	- compression LZ77 start
 * 			compressor_cmd_lz77_append	- new data to compress
 * 			compressor_cmd_lz77_end		- end compression
 */
inline void compressor_lz77_cmd(lz77_sub_cmd_t sub_cmd, uint8_t *data, size_t size)
{
	module.input.cmd 			= uart_cmd_lz77;
	module.input.sub_cmd.lz77 	= sub_cmd;
	module.input.data			= data;
	module.input.count 			= size;

	osSemaphoreRelease(module.cmd_request);
}


void compressor_lzss_cmd(lzss_sub_cmd_t sub_cmd, uint8_t *data, size_t size)
{
	module.input.cmd 			= uart_cmd_lzss;
	module.input.sub_cmd.lzss 	= sub_cmd;
	module.input.data			= data;
	module.input.count 			= size;

	osSemaphoreRelease(module.cmd_request);
}
/**
 * 	@brief init function for module, should be called when task start
 */
inline static void init(void)
{
	module.cmd_request 	= osSemaphoreCreate(osSemaphore(DATA_TO_COMPRESS_RECEIVED), 1);
	module.lz77_handle 	= NULL;
	module.lzss_handle 	= NULL;

	osSemaphoreWait(module.cmd_request, osWaitForever);
}


static void lz77_service(void)
{
	sub_cmd_t sub_cmd;
	sub_cmd.lz77 = module.input.sub_cmd.lz77;

	switch(sub_cmd.lz77)
	{
	case compressor_cmd_lz77_start:
		if(module.lz77_handle != NULL) lz77_destroy(module.lz77_handle);
		module.lz77_handle = lz77_create();
		break;

	case compressor_cmd_lz77_append:
		received_data += module.input.count;
		module.output.count = lz77_compress(module.lz77_handle,
											module.input.data,
											module.input.count,
											module.output.data);
		uart_transmitter_send( uart_cmd_lz77, sub_cmd, module.output.data, module.output.count);
		return;

	case compressor_cmd_lz77_end:
		if(module.lz77_handle != NULL) lz77_destroy(module.lz77_handle);
		module.lz77_handle = NULL;
		break;

	case compressor_cmd_lz77_send_again:
		uart_transmitter_send( uart_cmd_lz77, sub_cmd, module.output.data, module.output.count);
		return;

	default:
		uart_transmitter_send( uart_cmd_lz77, sub_cmd, (uint8_t *)MESSAGE_ERROR, sizeof(MESSAGE_ERROR) - 1);
		return;

	}

	uart_transmitter_send( uart_cmd_lz77, sub_cmd, (uint8_t *)MESSAGE_OK, sizeof(MESSAGE_OK) - 1);
}


static void lzss_service(void)
{
	sub_cmd_t sub_cmd;
	sub_cmd.lzss = module.input.sub_cmd.lzss;

	switch(sub_cmd.lzss)
	{
	case compressor_cmd_lzss_start:
		if(module.lzss_handle != NULL) lzss_destroy(module.lzss_handle);
		module.lzss_handle  = lzss_create();
		break;

	case compressor_cmd_lzss_append:
		received_data += module.input.count;
		module.output.count = lzss_compress(module.lzss_handle,
											module.input.data,
											module.input.count,
											module.output.data);
		uart_transmitter_send( uart_cmd_lzss, sub_cmd, module.output.data, module.output.count);
		return;

	case compressor_cmd_lzss_end:
		if(module.lzss_handle != NULL) lzss_destroy(module.lzss_handle);
		module.lzss_handle = NULL;
		break;

	case compressor_cmd_lzss_send_again:
		uart_transmitter_send( uart_cmd_lzss, sub_cmd, module.output.data, module.output.count);
		return;

	default:
		uart_transmitter_send( uart_cmd_lzss, sub_cmd, (uint8_t *)MESSAGE_ERROR, sizeof(MESSAGE_ERROR) - 1);
		return;

	}

	uart_transmitter_send( uart_cmd_lz77, sub_cmd, (uint8_t *)MESSAGE_OK, sizeof(MESSAGE_OK) - 1);
}

