#include "uart_receiver.h"
#include "usart.h"
//#include "main.h"
#include "cmsis_os.h"

#include "compressor.h"
#include "frame.h"


static void init(void);
static void analyze_frame(void);

volatile static osSemaphoreId dataReceived;
osSemaphoreDef(RECEIVED_DATA_SEM);

volatile struct{
	frame_struct_t frame;
	uint8_t *rx_data_head;
	uint8_t data_buffer[300];

	uint16_t rx_index;
}uart_task_module;


/**
 * @brief append new data to received data
 * @param data new data to buffer
 */
inline void uart_reveiver_append_data(uint8_t data)
{
	if(uart_task_module.rx_index  == 0)
	{
		osSemaphoreRelease(dataReceived);
	}

	if(uart_task_module.rx_index < sizeof(uart_task_module.frame.head))
	{
		uart_task_module.rx_data_head[ uart_task_module.rx_index ] = data;
	}
	else
	{
		uart_task_module.data_buffer[uart_task_module.rx_index - sizeof(uart_task_module.frame.head)] = data;
	}

	uart_task_module.rx_index++;
}

/**
 *  @brief Task for receiving data from UART. At the end frame is analyzed and sended to module.
 *  @param args argument passed to task, there only NULL
 */
void uart_receiver_task(void const *args)
{
	uint8_t last_index = 0;
	init();

	while(1)
	{
		osSemaphoreWait(dataReceived, osWaitForever);

		while(last_index != uart_task_module.rx_index)
		{
			last_index = uart_task_module.rx_index;
			osDelay(10);
		}

		analyze_frame();
	}

}

void init(void)
{
	dataReceived = osSemaphoreCreate(osSemaphore(RECEIVED_DATA_SEM), 1);

	//Block semaphore
	osSemaphoreWait(dataReceived, osWaitForever);

	uart_task_module.rx_index 		= 0;
	uart_task_module.rx_data_head 	= (uint8_t *)&uart_task_module.frame;
	uart_task_module.frame.data 	= uart_task_module.data_buffer;
}

static void analyze_frame(void)
{
	uint8_t cmd 		= uart_task_module.frame.head.cmd;
	sub_cmd_t sub_cmd 	= uart_task_module.frame.head.sub_cmd;
	uint8_t *data_ptr	= uart_task_module.frame.data;
	uint8_t count		= uart_task_module.rx_index - sizeof(uart_task_module.frame.head);

	//TODO Decoding frame, checking CRC, send it to module
	uart_task_module.rx_index = 0;

	switch(cmd)
	{
	case uart_cmd_lz77:	compressor_lz77_cmd(sub_cmd.lz77, data_ptr, count); break;
	case uart_cmd_lzss: compressor_lzss_cmd(sub_cmd.lz77, data_ptr, count); break;

	default:
		break;

	}


}

