/*
 * heart_beater.h
 *
 *  Created on: Nov 11, 2021
 *      Author: robsk
 */

#ifndef INC_TASKS_HEART_BEATER_H_
#define INC_TASKS_HEART_BEATER_H_

void heart_beater_task(void const *args);

#endif /* INC_TASKS_HEART_BEATER_H_ */
