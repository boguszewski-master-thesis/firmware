#ifndef __USART_TASK_H__
#define __USART_TASK_H__

#include "main.h"

void uart_reveiver_append_data(uint8_t);
void uart_receiver_task(void const *args);

#endif //__USART_TASK_H__
