/*
 * compressor.h
 *
 *  Created on: 12 Nov 2021
 *      Author: robsk
 */

#ifndef INC_COMPRESSOR_H_
#define INC_COMPRESSOR_H_

#include <stdint.h>
#include <stddef.h>

/**
 * @brief struct describe sub-command connected with uart_cmd_lz77 command
 */
typedef enum lz77_sub_cmd{
	compressor_cmd_lz77_start,
	compressor_cmd_lz77_append,
	compressor_cmd_lz77_end,
	compressor_cmd_lz77_send_again
}lz77_sub_cmd_t;

typedef enum lzss_sub_cmd{
	compressor_cmd_lzss_start,
	compressor_cmd_lzss_append,
	compressor_cmd_lzss_end,
	compressor_cmd_lzss_send_again
}lzss_sub_cmd_t;


void compressor_task(void const *args);

void compressor_lz77_cmd(lz77_sub_cmd_t sub_cmd, uint8_t *data, size_t size);
void compressor_lzss_cmd(lzss_sub_cmd_t sub_cmd, uint8_t *data, size_t size);

#endif /* INC_COMPRESSOR_H_ */
