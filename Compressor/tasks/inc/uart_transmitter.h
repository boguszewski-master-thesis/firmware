/*
 * uart_transmitter.h
 *
 *  Created on: 15 Nov 2021
 *      Author: robsk
 */

#ifndef INC_UART_TRANSMITTER_H_
#define INC_UART_TRANSMITTER_H_

#include "frame.h"

void uart_transmitter_task(void const *args);
void uart_transmitter_send(cmd_t cmd, sub_cmd_t sub_cmd, uint8_t *data, size_t count_data);

void uart_transmitter_tx_complete(void);

#endif /* INC_UART_TRANSMITTER_H_ */
